# Calculette

Calculatrice en NodeJS développé dans le cadre du TP Tests-Unitaires, le but est de mettre en place des tests en BDD dans un premier temps, puis en TDD.
### Installation et utilisation

Une fois le projet téléchargé, lancez la commande 
```
npm install
```
Pour utiliser la calculatrice entrez la commande
```
npm run calculator
```
Pour lancer les tests unitaires, entrez la commande
```
npm run test
```

#BDD : 
- Rédiger une description du comportement attendu de la calculatrice en utilisant une syntaxe spécifique.
- Exécuter les cas de test et vérifier que le comportement de la calculatrice correspond à ce qui est décrit dans la description.

#TDD : 
- 1. Additionner deux nombres :
Tester si 2+1=3.

- 2. Multiplier deux nombres :
Tester si 4*4=16.

- 3. Soustraire deux nombres : 
Tester si 5-2=3.

- 4. Diviser deux nombres :
Tester si 10/2=5.

- 5. Le pourcentage
Tester si 10%=0.1.

- 6. Additionner deux nombres décimaux :
Tester si 5.2 + 4.1 = 9.3

- 7. Multiplier deux nombres décimaux :
Tester si 2.5 * 6.3 = 15.75

- 8. Diviser deux nombres décimaux :
Tester si 12.5/5.2 = 2.4038461538461537

- 9. Division 0 :
Tester 5/0 = Infinity

- 10. Les puissances de deux nombres :
Tester si 3^4 = 81

- 11. La puissance 2 d'un nombre :
Tester si 10^2 = 100
